#include <iostream>
#include <thread>
#include <Connection.h>

#define SERVER_PORT 60000
#define TICK_RATE 10
#define SLEEP 1000000 / TICK_RATE

void displayOptions();

void networking( net::Connection *connection );

int main()
{
	printf( "Enter player name: " );
	std::string playerName;
	std::getline( std::cin, playerName );
	if ( playerName.empty())
	{
		printf( "You must enter a name!" );
		return 1;
	}

	std::string input;
	printf( "Enter server IP or hit enter for 127.0.0.1\n" );
	std::getline( std::cin, input );
	if ( input.empty())
	{
		input = "127.0.0.1";
	}

	net::Connection connection( input, SERVER_PORT, 1 );
	std::thread networkThread( networking, &connection );
	while ( !connection.isConnected())
	{
		printf( "Waiting until connection available\n" );
		usleep( SLEEP );
	}

	connection.getPlayerManager().SendPlayerCreate( playerName );
	displayOptions();
	while ( connection.isConnected())
	{
		std::cin >> input;
		if ( input == "1" )
		{
			printf( "Enter message to send: " );
			std::cin.ignore( std::numeric_limits< std::streamsize >::max(), '\n' );
			std::getline( std::cin, input );
			connection.SendMessage( connection.getHost(), playerName, input );
		}

		if ( input == "2" )
		{
			connection.Disconnect();
		}
		else
		{
			displayOptions();
		}
		usleep( SLEEP );
	}

	networkThread.join();

	return 0;
}

void displayOptions()
{
	printf( "Choose an option\n" );
	printf( "1) Send Message\n" );
	printf( "2) Leave Server\n" );
	printf( "Choice: " );
}

void networking( net::Connection *connection )
{
	bool running = true;
	bool startup = true;
	while ( running )
	{
		running = !connection->ReceivePackets() && ( connection->isConnected() || startup );

		if ( connection->isConnected())
		{
			startup = false;
		}
		usleep( SLEEP );
	}
}