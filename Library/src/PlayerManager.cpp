#include <raknet/BitStream.h>
#include <raknet/PacketPriority.h>
#include <raknet/RakNetTypes.h>
#include "../includes/PlayerManager.h"
#include "../includes/Messages.h"

namespace net
{
	PlayerManager::PlayerManager()
			: peer( nullptr ), currentPlayer( nullptr )
	{

	}

	void PlayerManager::SetRakPeerInterface( RakNet::RakPeerInterface *peer )
	{
		this->peer = peer;
	}

	void PlayerManager::SetHostAddress( RakNet::SystemAddress hostAddress )
	{
		this->hostAddress = hostAddress;
	}

	Player PlayerManager::AddPlayer( std::string playerName, RakNet::RakNetGUID *playerGUID )
	{
		Player player( &networkIDManager, 0, playerName );
		if ( playerGUID != nullptr )
		{
			player.SetGUID( *playerGUID );
		}
		playerList.insert( std::pair< RakNet::NetworkID, Player >( player.GetNetworkID(), player ));
		return player;
	}

	void PlayerManager::AddPlayer( std::string playerName, RakNet::NetworkID playerID )
	{
		Player player( &networkIDManager, 0, playerName );
		player.SetNetworkID( playerID );
		auto iter = playerList.find( player.GetNetworkID());
		if ( iter == playerList.end())
		{
			playerList.insert( std::pair< RakNet::NetworkID, Player >( player.GetNetworkID(), player ));

			if ( currentPlayer == nullptr )
			{
				iter = playerList.find( player.GetNetworkID());
				currentPlayer = &iter->second;
			}
		}
	}

	RakNet::RakNetGUID PlayerManager::GetPlayerGUID( RakNet::NetworkID playerID )
	{
		return playerList.at( playerID ).GetGUID();
	}

	std::string PlayerManager::RemovePlayer( RakNet::NetworkID playerID )
	{
		auto iter = playerList.find( playerID );
		if ( iter != playerList.end())
		{
			std::string playerName = iter->second.GetName();
			playerList.erase( iter );
			return playerName;
		}
		return "Unknown player";
	}

	void PlayerManager::RemovePlayer( RakNet::RakNetGUID playerGUID )
	{
		auto iter = playerList.begin();
		while ( iter != playerList.end())
		{
			if ( iter->second.GetGUID() == playerGUID )
			{
				Player player = iter->second;
				playerList.erase( iter );
				BroadcastPlayerLeave( player );
				printf( "Player %s has left the server.\n", player.GetName().c_str());
				break;
			}
			iter++;
		}
	}

	void PlayerManager::SendPlayerList( RakNet::SystemAddress destination )
	{
		RakNet::BitStream bsOut;
		RakNet::RakNetGUID dest = peer->GetGuidFromSystemAddress( destination );
		bsOut.Write( static_cast<RakNet::MessageID>( ID_PLAYER_LIST ));
		bsOut.Write( static_cast<uint16_t>(playerList.size()));
		for ( auto iter = playerList.begin(); iter != playerList.end(); iter++ )
		{
			bsOut.Write( iter->second.GetName().c_str());
			bsOut.Write( iter->second.GetNetworkID());
		}

		peer->Send( &bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, destination, false );
	}

	void PlayerManager::BroadcastPlayerJoin( Player player )
	{
		RakNet::BitStream bsOut;
		bsOut.Write( static_cast<RakNet::MessageID>( ID_PLAYER_JOIN ));
		bsOut.Write( player.GetName().c_str());
		bsOut.Write( player.GetNetworkID());

		peer->Send( &bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true );
	}

	void PlayerManager::BroadcastPlayerLeave( Player player )
	{
		RakNet::BitStream bsOut;
		bsOut.Write( static_cast<RakNet::MessageID>( ID_PLAYER_LEAVE ));
		bsOut.Write( player.GetNetworkID());

		peer->Send( &bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, player.GetGUID(), true );
	}

	void PlayerManager::SendPlayerCreate( std::string playerName )
	{
		RakNet::BitStream bsOut;
		bsOut.Write( static_cast<RakNet::MessageID>( ID_PLAYER_CREATE ));
		bsOut.Write( playerName.c_str());

		peer->Send( &bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, hostAddress, false );
	}

	Player *PlayerManager::getCurrentPlayer()
	{
		return currentPlayer;
	}


}