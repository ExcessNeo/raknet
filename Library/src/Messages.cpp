#include <cstdio>
#include <raknet/RakString.h>
#include <raknet/BitStream.h>
#include "../includes/Messages.h"
#include "../includes/Connection.h"

namespace net
{
	void HandleMessage( Connection *connection, RakNet::Packet *packet )
	{
		switch ( packet->data[ 0 ] )
		{
			case ID_CHAT_MSG:
			{
				RakNet::RakString name, msg;
				RakNet::BitStream bsIn( packet->data, packet->length, false );
				bsIn.IgnoreBytes( sizeof( RakNet::MessageID ));
				bsIn.Read( name );
				bsIn.Read( msg );
				printf( "%s: %s\n", name.C_String(), msg.C_String());

				if ( connection->isHost())
				{
					connection->BroadcastMessage( name.C_String(), msg.C_String());
				}
			}
				break;

			case ID_PLAYER_CREATE:
			{
				RakNet::BitStream bsIn( packet->data, packet->length, false );
				bsIn.IgnoreBytes( sizeof( RakNet::MessageID ));
				RakNet::RakString playerName;
				bsIn.Read( playerName );
				Player player = connection->getPlayerManager().AddPlayer( playerName.C_String(), &packet->guid );

				printf( "Player %s joined the server.\n", playerName.C_String());
				connection->getPlayerManager().BroadcastPlayerJoin( player );
				connection->getPlayerManager().SendPlayerList( packet->systemAddress );
			}
				break;

			case ID_PLAYER_JOIN:
			{
				RakNet::BitStream bsIn( packet->data, packet->length, false );
				bsIn.IgnoreBytes( sizeof( RakNet::MessageID ));
				RakNet::RakString playerName;
				RakNet::NetworkID playerID;
				bsIn.Read( playerName );
				bsIn.Read( playerID );

				connection->getPlayerManager().AddPlayer( playerName.C_String(), playerID );
				printf( "Player %s joined the server.\n", playerName.C_String());
			}
				break;

			case ID_PLAYER_LEAVE:
			{
				RakNet::BitStream bsIn( packet->data, packet->length, false );
				bsIn.IgnoreBytes( sizeof( RakNet::MessageID ));
				RakNet::NetworkID networkID;
				bsIn.Read( networkID );
				std::string playerName = connection->getPlayerManager().RemovePlayer( networkID );

				printf( "Player %s left the server.\n", playerName.c_str());
			}
				break;

			case ID_PLAYER_LIST:
			{
				RakNet::BitStream bsIn( packet->data, packet->length, false );
				bsIn.IgnoreBytes( sizeof( RakNet::MessageID ));
				uint16_t players;
				bsIn.Read( players );

				printf( "There are %i players on the server.\n", players );
				for ( int i = 0; i < players; i++ )
				{
					RakNet::RakString playerName;
					RakNet::NetworkID playerID;
					bsIn.Read( playerName );
					bsIn.Read( playerID );

					connection->getPlayerManager().AddPlayer( playerName.C_String(), playerID );
					printf( "Got info for player %s on server.\n", playerName.C_String());
				}
			}
				break;

			case ID_GOODBYE:
				break;

			default:
				printf( "Message with identifier %i unhandled.\n", packet->data[ 0 ] );
		}
	}
}
