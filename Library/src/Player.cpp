#include "../includes/Player.h"

namespace net
{
	Player::Player( RakNet::NetworkIDManager *networkIDManager, uint64_t joinTime, std::string name )
			: joinTime( joinTime ), name( name )
	{
		SetNetworkIDManager( networkIDManager );
	}

	void Player::SetGUID( RakNet::RakNetGUID playerGUID )
	{
		this->playerGUID = playerGUID;
	}

	RakNet::RakNetGUID Player::GetGUID()
	{
		return playerGUID;
	}

	void Player::SetName( std::string name )
	{
		this->name = name;
	}

	std::string Player::GetName()
	{
		return name;
	}
}
