#include <raknet/RakNetTypes.h>
#include <raknet/BitStream.h>
#include "../includes/Connection.h"

namespace net
{
	Connection::Connection()
			: Connection( DEFAULT_SERVER_PORT, DEFAULT_MAX_CLIENTS )
	{

	}

	Connection::Connection( uint16_t port, uint16_t connections )
			: socketDescriptor( port, 0 ), server( true ), connected( false )
	{
		printf( "Starting in server mode.\n" );

		peer = RakNet::RakPeerInterface::GetInstance();
		peer->Startup( connections, &socketDescriptor, 1 );
		peer->SetMaximumIncomingConnections( connections );
		playerManager.SetRakPeerInterface( peer );
	}

	Connection::Connection( std::string host, uint16_t port, uint16_t connections )
			: host( host ), server( false ), hostAddress( host.c_str()), connected( false )
	{
		peer = RakNet::RakPeerInterface::GetInstance();
		playerManager.SetRakPeerInterface( peer );
		peer->Startup( connections, &socketDescriptor, 1 );

		printf( "Connecting to host %s:%i.\n", host.c_str(), port );
		peer->Connect( host.c_str(), port, 0, 0 );
	}

	Connection::~Connection()
	{
		peer->Shutdown( 300 );
		RakNet::RakPeerInterface::DestroyInstance( peer );
	}

	bool Connection::ReceivePackets()
	{
		bool closed = false;
		RakNet::Packet *packet = nullptr;
		for ( packet = peer->Receive(); packet != nullptr; peer->DeallocatePacket( packet ), packet = peer->Receive())
		{
			switch ( packet->data[ 0 ] )
			{
				case ID_REMOTE_DISCONNECTION_NOTIFICATION:
					printf( "Another client has disconnected.\n" );
					break;

				case ID_REMOTE_CONNECTION_LOST:
					printf( "Another client has lost the connection.\n" );
					break;

				case ID_REMOTE_NEW_INCOMING_CONNECTION:
					printf( "Another client has connected.\n" );
					break;

				case ID_CONNECTION_REQUEST_ACCEPTED:
				{
					printf( "Our connection request has been accepted.\n" );
					hostAddress = packet->systemAddress;
					playerManager.SetHostAddress( hostAddress );
					connected = true;
				}
					break;

				case ID_NEW_INCOMING_CONNECTION:
					printf( "A connection is incoming.\n" );
					break;

				case ID_NO_FREE_INCOMING_CONNECTIONS:
					printf( "The server is full.\n" );
					break;

				case ID_DISCONNECTION_NOTIFICATION:
					if ( !server )
					{
						printf( "We have been disconneced.\n" );
						closed = true;
						connected = false;
					}
					else
					{
						playerManager.RemovePlayer( packet->guid );
					}
					break;

				case ID_CONNECTION_LOST:
					printf( "Connection lost.\n" );
					if ( !server )
					{
						closed = true;
						connected = false;
					}
					else
					{
						playerManager.RemovePlayer( packet->guid );
					}
					break;

				case ID_SND_RECEIPT_ACKED:
					printf( "Ack received.\n" );
					break;

				default:
//					printf( "Message with identifier %i has arrived, passing to custom message handler.\n",
//							packet->data[ 0 ] );
					HandleMessage( this, packet );
					break;
			}
		}
		return closed;
	}

	void Connection::SendMessage( RakNet::SystemAddress destination, std::string name, std::string message )
	{
		RakNet::BitStream bsOut;
		bsOut.Write(( RakNet::MessageID ) ID_CHAT_MSG );
		bsOut.Write( name.c_str());
		bsOut.Write( message.c_str());
		peer->Send( &bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, hostAddress, false );
	}

	void Connection::BroadcastMessage( std::string name, std::string message )
	{
		RakNet::BitStream bsOut;
		bsOut.Write(( RakNet::MessageID ) ID_CHAT_MSG );
		bsOut.Write( name.c_str());
		bsOut.Write( message.c_str());
		peer->Send( &bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true );
	}

	PlayerManager &Connection::getPlayerManager()
	{
		return playerManager;
	}

	RakNet::SystemAddress Connection::getHost()
	{
		return hostAddress;
	}

	void Connection::Disconnect()
	{
		connected = false;
		peer->CloseConnection( hostAddress, true );
	}

	bool Connection::isConnected()
	{
		return connected;
	}

	bool Connection::isHost()
	{
		return server;
	}
}