#ifndef RAKNET_PLAYER_H
#define RAKNET_PLAYER_H

#include <string>
#include <raknet/RakNetTypes.h>
#include <raknet/NetworkIDObject.h>

namespace net
{
	class Player : public RakNet::NetworkIDObject
	{
	public:
		Player( RakNet::NetworkIDManager *networkIDManager, uint64_t joinTime, std::string name );

		void SetGUID( RakNet::RakNetGUID playerGUID );

		RakNet::RakNetGUID GetGUID();

		void SetName( std::string name );

		std::string GetName();

	protected:
		RakNet::RakNetGUID playerGUID;
		uint64_t joinTime;
		std::string name;
	};
}

#endif
