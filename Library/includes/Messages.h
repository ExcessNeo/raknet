#ifndef RAKNET_MESSAGES_H
#define RAKNET_MESSAGES_H

#include <raknet/MessageIdentifiers.h>
#include <raknet/RakNetTypes.h>

namespace net
{
	class Connection;

	enum GameMessages
	{
		ID_PLAYER_CREATE = ID_USER_PACKET_ENUM + 1,
		ID_PLAYER_JOIN,
		ID_PLAYER_LEAVE,
		ID_PLAYER_LIST,
		ID_CHAT_MSG,

		ID_GOODBYE
	};

	void HandleMessage( Connection *connection, RakNet::Packet *packet );
}

#endif
