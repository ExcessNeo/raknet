#ifndef RAKNET_CONNECTION_H
#define RAKNET_CONNECTION_H

#define DEFAULT_SERVER_PORT 60000
#define DEFAULT_MAX_CLIENTS 8

#include <atomic>
#include <mutex>
#include <cstdint>
#include <string>
#include <unordered_map>
#include <raknet/RakPeerInterface.h>
#include "Messages.h"
#include "Player.h"
#include "PlayerManager.h"

namespace net
{
	class Connection
	{
	public:
		Connection();

		Connection( uint16_t port, uint16_t connections );

		Connection( std::string host, uint16_t port, uint16_t connections = 1 );

		~Connection();

		bool ReceivePackets();

		void SendMessage( RakNet::SystemAddress destination, std::string name, std::string message );

		void BroadcastMessage( std::string name, std::string message );

		PlayerManager &getPlayerManager();

		RakNet::SystemAddress getHost();

		void Disconnect();

		bool isConnected();

		bool isHost();

	protected:
		RakNet::RakPeerInterface *peer;
		RakNet::SocketDescriptor socketDescriptor;
		RakNet::SystemAddress hostAddress;
		PlayerManager playerManager;
		std::string host;
		bool server;
		std::mutex connectedMutex;
		std::atomic< bool > connected;
	};
}

#endif
