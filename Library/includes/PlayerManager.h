#ifndef RAKNET_PLAYERMANAGER_H
#define RAKNET_PLAYERMANAGER_H

#include <unordered_map>
#include <raknet/NetworkIDManager.h>
#include <raknet/RakPeerInterface.h>
#include "Player.h"

namespace net
{
	class PlayerManager
	{
	public:
		PlayerManager();

		void SetRakPeerInterface( RakNet::RakPeerInterface *peer );

		void SetHostAddress( RakNet::SystemAddress hostAddress );

		Player AddPlayer( std::string playerName, RakNet::RakNetGUID *playerGUID = nullptr );

		void AddPlayer( std::string playerName, RakNet::NetworkID playerID );

		RakNet::RakNetGUID GetPlayerGUID( RakNet::NetworkID playerID );

		std::string RemovePlayer( RakNet::NetworkID playerID );

		void RemovePlayer( RakNet::RakNetGUID playerGUID );

		void SendPlayerList( RakNet::SystemAddress destination );

		void BroadcastPlayerJoin( Player player );

		void BroadcastPlayerLeave( Player player );

		void SendPlayerCreate( std::string playerName );

		Player *getCurrentPlayer();

	private:
		RakNet::NetworkIDManager networkIDManager;
		RakNet::RakPeerInterface *peer;
		RakNet::SystemAddress hostAddress;
		std::unordered_map< RakNet::NetworkID, Player > playerList;
		Player *currentPlayer;
	};
}

#endif