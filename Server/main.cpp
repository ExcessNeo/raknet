#include <iostream>
#include <cstdio>
#include <Connection.h>

#define MAX_CLIENTS 10
#define SERVER_PORT 60000
#define TICK_RATE 10
#define SLEEP 1000000 / TICK_RATE

int main()
{
	net::Connection connection( SERVER_PORT, MAX_CLIENTS );
	bool running = true;
	while ( running )
	{
		running = !connection.ReceivePackets();
		usleep( SLEEP );
	}
	printf( "Closing down.\n" );
	return 0;
}